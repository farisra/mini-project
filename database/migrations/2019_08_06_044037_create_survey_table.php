<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->bigInteger('nik');
            $table->integer('tanggal');
            $table->unsignedInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('gender')->onDelete('cascade');
            $table->unsignedInteger('study_id');
            $table->foreign('study_id')->references('id')->on('study')->onDelete('cascade');
            $table->unsignedInteger('status_id');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey');
    }
}
