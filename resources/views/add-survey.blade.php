@extends('main', ['currentMenu' => 'mn-survey'])
@section('content')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Data Survey
                <div class="page-title-subheading">
                    Your personal data bellow
                </div>
            </div>
        </div> 
</div>
</div>            
<div class="main-card mb-3 card">
    <div class="card-body">
        <h5 class="card-title">Details Survey</h5>
        <form action="{{$action}}" method="post" class="needs-validation" novalidate>
            @if($method == 1)
                @method('PUT')
            @endif
            @csrf
            <div class="position-relative row form-group">
                <label for="in-su-nama" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="nama" id="in-su-nama" placeholder="" 
                    type="text" class="form-control form-control-sm" value="{{$survey->nama}}" required>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="in-su-nik" class="col-sm-2 col-form-label">NIK</label>
                <div class="col-sm-10">
                    <input name="nik" id="in-su-nik" placeholder="" 
                    type="number" class="form-control form-control-sm" value="{{$survey->nik}}" required>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="in-su-tgl" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                <div class="col-sm-10">
                    <input name="tanggal" id="in-su-tgl" placeholder="" 
                    type="date" class="form-control form-control-sm" value="{{$survey->tanggal}}" required>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="sel-gen" class="col-sm-2 col-form-label">Gender</label>
                <div class="col-sm-10">
                    <select name="gender" id="sel-gen" class="chosen form-control form-control-sm">
                        @foreach($component->gender as $gen)
                            @if($survey->gender && $survey->gender->id == $gen->id)
                                <option value="{{$gen->id}}" selected>{{$gen->description}}</option>
                            @else
                                <option value="{{$gen->id}}">{{$gen->description}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="sel-std" class="col-sm-2 col-form-label">Pendidikan Terakhir</label>
                <div class="col-sm-10">
                    <select name="study" id="sel-std" class="chosen form-control form-control-sm">
                        @foreach($component->study as $std)
                            @if($survey->study && $survey->study->id == $std->id)
                                <option value="{{$std->id}}" selected>{{$std->description}}</option>
                            @else
                                <option value="{{$std->id}}">{{$std->description}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                    <label class="form-check-label" for="invalidCheck">
                        I have correctly entered the data. 
                    </label>
                    <div class="invalid-feedback">
                        You must agree before submitting.
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit</button>
        </form>

        <script>
            // Example starter JavaScript for disabling form submissions if there are invalid fields
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    var forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>
    </div>
</div>
@endsection