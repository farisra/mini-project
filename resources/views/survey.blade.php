@extends('main', ['currentMenu' => 'mn-survey'])
@section('content')
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Data Survey</div>
                </div>   
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Survey</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{count($survey)}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Survey 
                        @if(Auth::user() && Auth::user()->role_id == 2)
                        <a href="{{route('survey.create')}}" class="btn btn-primary ml-auto"><i class="fa fa-plus-circle"></i> Add Survey</a>
                        @endif
                    </div>
                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover datatable-custom">
                            <thead>
                            <tr>
                                <th class="text-center">No</th>
                                <th>Name</th>
                                <th class="text-center">NIK</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if($survey)
                                    @foreach($survey as $s)
                                    <tr>
                                        <td class="text-center text-muted">1</td>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left flex2">
                                                        <div class="widget-heading">{{$s->nama}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center">{{$s->nik}}</td>
                                        <td class="text-center">
                                            @if($s->status->id == 1)
                                                <div class="badge badge-secondary">
                                            @elseif($s->status->id == 2)
                                                <div class="badge badge-success">
                                            @else
                                                <div class="badge badge-danger">
                                            @endif
                                                {{$s->status->description}}
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            @if(Auth::user() && Auth::user()->role_id == 2)
                                            <a href="{{route('survey.edit', $s->id)}}" class="btn badge badge-primary text-white">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @endif
                                            <button type="button" class="btn badge badge-primary btn-detail-survey" data-survey-id="{{$s->id}}" 
                                            data-toggle="modal" data-target=".detail-survey">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection