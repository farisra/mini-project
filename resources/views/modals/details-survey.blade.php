<div class="modal fade detail-survey" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Details Survey</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-card mb-12 card">
                            <div class="card-body">
                                <h5 class="card-title">Survey</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="active list-group-item-action list-group-item">Name</p>
                                        <p class="list-group-item-action list-group-item su-nama"></p>
                                        <p class="active list-group-item-action list-group-item">NIK</p>
                                        <p class="list-group-item-action list-group-item su-nik"></p>
                                        <p class="active list-group-item-action list-group-item">Tanggal Lahir</p>
                                        <p class="list-group-item-action list-group-item su-tgl"></p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="active list-group-item-action list-group-item">Gender</p>
                                        <p class="list-group-item-action list-group-item su-gen"></p>
                                        <p class="active list-group-item-action list-group-item">Pendidikan Terakhir</p>
                                        <p class="list-group-item-action list-group-item su-std"></p>
                                        <p class="active list-group-item-action list-group-item">Status</p>
                                        <p class="list-group-item-action list-group-item su-status pl-3 badge badge-focus"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                @if(Auth::user() && Auth::user()->role_id == 1)
                    <button type="button" class="btn btn-danger set-id su-reject">Reject</button>
                    <button type="button" class="btn btn-success set-id su-approve">Approve</button>
                @endif
            </div>
        </div>
    </div>
</div>