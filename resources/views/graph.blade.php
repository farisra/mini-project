@extends('main', ['currentMenu' => 'mn-graph'])
@section('content')
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Data Survey</div>
                </div>   
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Grafik</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header">Survey by Year 
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <canvas id="graph-1" width="200" height="100"></canvas>
                        </div>
                    </div>
                </div>
                <div class="main-card mb-3 card">
                    <div class="card-header">Survey by Gender
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <canvas id="graph-2" width="200" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection