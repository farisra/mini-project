$(document).ready(function() {
    $("a#" + currentMenu).addClass("mm-active");

    alertSession = $("#alert-session");
    if (alertSession.length != 0) {
        alertSession.fadeTo(2000, 500).slideUp(500, function() {
            alertSession.slideUp(300);
        });
    }

    var t = $('.datatable-custom').DataTable({
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [
            [1, 'asc']
        ]
    });

    t.on('order.dt search.dt', function() {
        t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('.btn-detail-survey').click(function() {
        id = $(this).data('survey-id');
        $.get("/survey/" + id, function(data) {
            console.log(data.status.id)
            $('.set-id').css("display", "none");

            $('.detail-survey .su-nama').text(data.nama);
            $('.detail-survey .su-nik').text(data.nik);
            $('.detail-survey .su-tgl').text(data.tanggal);
            $('.detail-survey .su-gen').text(data.gender.description);
            $('.detail-survey .su-std').text(data.study.description);
            $('.detail-survey .su-status').text(data.status.description);

            if (data.status.id == 1) {
                $('.su-reject').css("display", "block");
                $('.set-id').data("su-id", data.id);
                $('.su-approve').css("display", "block");
                $('.set-id').data("su-id", data.id);
            }

            $('.su-status').removeClass($('.su-status').attr('class').split(' ').pop());
            switch (data.status.id) {
                case 1:
                    $('.su-status').addClass("badge-secondary");
                    break;
                case 2:
                    $('.su-status').addClass("badge-success");
                    break;
                default:
                    $('.su-status').addClass("badge-danger");
            }
        });
    })

    $('.su-reject').click(function() {
        if (confirm("Do you seriously want to reject this survey?"))
            document.location = "/survey/3/" + $(this).data('su-id');
    })

    $('.su-approve').click(function() {
        if (confirm("Do you seriously want to approve this survey?"))
            document.location = "/survey/2/" + $(this).data('su-id');
    })
})