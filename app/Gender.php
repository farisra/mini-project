<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table ='gender';
    public function survey()
    {
        return $this->hasMany('App\Survey', 'gender_id', 'id');
    }
}
