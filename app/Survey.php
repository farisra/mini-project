<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table ='survey';
    public $timestamps = false;
    protected $fillable = [
        'id', 'nama', 'tanggal', 'gender_id', 'study_id', 'status_id', 'nik'
    ];
    public function gender()
    {
        return $this->belongsTo('App\Gender', 'gender_id');
    }
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }
    public function study()
    {
        return $this->belongsTo('App\Study', 'study_id');
    }
}
