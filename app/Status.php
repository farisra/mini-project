<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table ='status';
    public function survey()
    {
        return $this->hasMany('App\Survey', 'status_id', 'id');
    }
}
