<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $table ='study';
    public function survey()
    {
        return $this->hasMany('App\Survey', 'study_id', 'id');
    }
}
