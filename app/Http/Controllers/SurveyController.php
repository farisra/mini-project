<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Gender;
use App\Study;

use DB;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $survey = Survey::all();
        return view('survey', compact('survey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $survey = new Survey();
        $component = $this->initComponent();
        $method = 2 ;
        $action = route('survey.index');
        return view('add-survey', compact('component', 'method', 'action', 'survey'));
    }

    public function initComponent(){
        $data = new \StdClass();
        $data->gender= Gender::all();
        $data->study= Study::all();
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $survey = new Survey();
        $survey->nama=$request->nama;
        $survey->nik=$request->nik;
        $survey->tanggal=strtotime($request->tanggal);
        $survey->gender_id=$request->gender;
        $survey->study_id=$request->study;
        $survey->status_id=1;
        if($survey->save()){
            return redirect('/survey');
        }
        return 'failed';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = Survey::where('id', $id)->with('gender', 'status', 'study')->get()->first();
        $survey->tanggal = date('d M Y', $survey->tanggal);
        return $survey;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = Survey::find($id);
        $survey->tanggal = date('Y-m-d', $survey->tanggal);
        $component = $this->initComponent();
        $method = 1 ;
        $action = route('survey.update', $id);
        return view('add-survey', compact('component', 'method', 'action', 'survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $survey = Survey::find($id);
        $survey->nama=$request->nama;
        $survey->nik=$request->nik;
        $survey->tanggal=strtotime($request->tanggal);
        $survey->gender_id=$request->gender;
        $survey->study_id=$request->study;
        if($survey->save()){
            return redirect('/survey');
        }
        return 'failed';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validasi($action,$id){
        $survey = Survey::find($id);
        $survey->status_id=$action;
        if($survey->save()){
            return redirect('/survey');
        }
        return 'failed';
    }

    public function surveyGraph($filter){
        if($filter == 1){
            $survey = Survey::select('gender_id', DB::raw('count(*) as total'))->groupBy('gender_id')->get();

        }else{
            $survey = Survey::select('gender_id', DB::raw('count(*) as total'))->groupBy('gender_id')->get();
        }
        return response()->json($survey);
    }
}
