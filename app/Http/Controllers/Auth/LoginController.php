<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/graph';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
 
    public function login(Request $request){

        $this->validate($request, [

            'username' => 'required',

            'password' => 'required',

            ]);

        if (\Auth::attempt([

            'username' => $request->username,

            'password' => $request->password])

        ){

            return redirect('/graph');

        }

        return redirect('/login')->with('error', 'Invalid Email address or Password');

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
