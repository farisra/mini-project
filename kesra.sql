toc.dat                                                                                             0000600 0004000 0002000 00000041045 13522244430 0014442 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP                            w            kesra    10.7    10.7 A    >           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false         ?           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false         @           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false         A           1262    20245    kesra    DATABASE     �   CREATE DATABASE kesra WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE kesra;
             postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false         B           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                     3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false         C           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1         �            1259    20289    gender    TABLE     i   CREATE TABLE public.gender (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.gender;
       public         postgres    false    3         �            1259    20287    gender_id_seq    SEQUENCE     �   CREATE SEQUENCE public.gender_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.gender_id_seq;
       public       postgres    false    204    3         D           0    0    gender_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.gender_id_seq OWNED BY public.gender.id;
            public       postgres    false    203         �            1259    20248 
   migrations    TABLE     �   CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);
    DROP TABLE public.migrations;
       public         postgres    false    3         �            1259    20246    migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.migrations_id_seq;
       public       postgres    false    197    3         E           0    0    migrations_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;
            public       postgres    false    196         �            1259    20267    password_resets    TABLE     �   CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);
 #   DROP TABLE public.password_resets;
       public         postgres    false    3         �            1259    20276    role    TABLE     g   CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.role;
       public         postgres    false    3         �            1259    20274    role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.role_id_seq;
       public       postgres    false    202    3         F           0    0    role_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;
            public       postgres    false    201         �            1259    20305    status    TABLE     i   CREATE TABLE public.status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.status;
       public         postgres    false    3         �            1259    20303    status_id_seq    SEQUENCE     �   CREATE SEQUENCE public.status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.status_id_seq;
       public       postgres    false    208    3         G           0    0    status_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;
            public       postgres    false    207         �            1259    20297    study    TABLE     h   CREATE TABLE public.study (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);
    DROP TABLE public.study;
       public         postgres    false    3         �            1259    20295    study_id_seq    SEQUENCE     �   CREATE SEQUENCE public.study_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.study_id_seq;
       public       postgres    false    3    206         H           0    0    study_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.study_id_seq OWNED BY public.study.id;
            public       postgres    false    205         �            1259    20313    survey    TABLE     �   CREATE TABLE public.survey (
    id integer NOT NULL,
    nama character varying(255) NOT NULL,
    nik bigint NOT NULL,
    tanggal integer NOT NULL,
    gender_id integer NOT NULL,
    study_id integer NOT NULL,
    status_id integer NOT NULL
);
    DROP TABLE public.survey;
       public         postgres    false    3         �            1259    20311    survey_id_seq    SEQUENCE     �   CREATE SEQUENCE public.survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.survey_id_seq;
       public       postgres    false    210    3         I           0    0    survey_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.survey_id_seq OWNED BY public.survey.id;
            public       postgres    false    209         �            1259    20256    users    TABLE     d  CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role_id integer NOT NULL
);
    DROP TABLE public.users;
       public         postgres    false    3         �            1259    20254    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    199    3         J           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    198         �
           2604    20292 	   gender id    DEFAULT     f   ALTER TABLE ONLY public.gender ALTER COLUMN id SET DEFAULT nextval('public.gender_id_seq'::regclass);
 8   ALTER TABLE public.gender ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    203    204         �
           2604    20251    migrations id    DEFAULT     n   ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);
 <   ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197         �
           2604    20279    role id    DEFAULT     b   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    202    201    202         �
           2604    20308 	   status id    DEFAULT     f   ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);
 8   ALTER TABLE public.status ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    208    208         �
           2604    20300    study id    DEFAULT     d   ALTER TABLE ONLY public.study ALTER COLUMN id SET DEFAULT nextval('public.study_id_seq'::regclass);
 7   ALTER TABLE public.study ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    206    205    206         �
           2604    20316 	   survey id    DEFAULT     f   ALTER TABLE ONLY public.survey ALTER COLUMN id SET DEFAULT nextval('public.survey_id_seq'::regclass);
 8   ALTER TABLE public.survey ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    210    209    210         �
           2604    20259    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199         5          0    20289    gender 
   TABLE DATA               1   COPY public.gender (id, description) FROM stdin;
    public       postgres    false    204       2869.dat .          0    20248 
   migrations 
   TABLE DATA               :   COPY public.migrations (id, migration, batch) FROM stdin;
    public       postgres    false    197       2862.dat 1          0    20267    password_resets 
   TABLE DATA               C   COPY public.password_resets (email, token, created_at) FROM stdin;
    public       postgres    false    200       2865.dat 3          0    20276    role 
   TABLE DATA               /   COPY public.role (id, description) FROM stdin;
    public       postgres    false    202       2867.dat 9          0    20305    status 
   TABLE DATA               1   COPY public.status (id, description) FROM stdin;
    public       postgres    false    208       2873.dat 7          0    20297    study 
   TABLE DATA               0   COPY public.study (id, description) FROM stdin;
    public       postgres    false    206       2871.dat ;          0    20313    survey 
   TABLE DATA               X   COPY public.survey (id, nama, nik, tanggal, gender_id, study_id, status_id) FROM stdin;
    public       postgres    false    210       2875.dat 0          0    20256    users 
   TABLE DATA               n   COPY public.users (id, name, username, password, remember_token, created_at, updated_at, role_id) FROM stdin;
    public       postgres    false    199       2864.dat K           0    0    gender_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.gender_id_seq', 1, false);
            public       postgres    false    203         L           0    0    migrations_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.migrations_id_seq', 8, true);
            public       postgres    false    196         M           0    0    role_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.role_id_seq', 1, false);
            public       postgres    false    201         N           0    0    status_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.status_id_seq', 1, false);
            public       postgres    false    207         O           0    0    study_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.study_id_seq', 1, false);
            public       postgres    false    205         P           0    0    survey_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.survey_id_seq', 6, true);
            public       postgres    false    209         Q           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 3, true);
            public       postgres    false    198         �
           2606    20294    gender gender_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.gender DROP CONSTRAINT gender_pkey;
       public         postgres    false    204         �
           2606    20253    migrations migrations_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
       public         postgres    false    197         �
           2606    20281    role role_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public         postgres    false    202         �
           2606    20310    status status_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
       public         postgres    false    208         �
           2606    20302    study study_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.study
    ADD CONSTRAINT study_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.study DROP CONSTRAINT study_pkey;
       public         postgres    false    206         �
           2606    20318    survey survey_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_pkey;
       public         postgres    false    210         �
           2606    20264    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    199         �
           2606    20266    users users_username_unique 
   CONSTRAINT     Z   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);
 E   ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_unique;
       public         postgres    false    199         �
           1259    20273    password_resets_email_index    INDEX     X   CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);
 /   DROP INDEX public.password_resets_email_index;
       public         postgres    false    200         �
           2606    20319    survey survey_gender_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_gender_id_foreign FOREIGN KEY (gender_id) REFERENCES public.gender(id) ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_gender_id_foreign;
       public       postgres    false    210    2729    204         �
           2606    20329    survey survey_status_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_status_id_foreign FOREIGN KEY (status_id) REFERENCES public.status(id) ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_status_id_foreign;
       public       postgres    false    210    208    2733         �
           2606    20324    survey survey_study_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_study_id_foreign FOREIGN KEY (study_id) REFERENCES public.study(id) ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_study_id_foreign;
       public       postgres    false    2731    206    210         �
           2606    20282    users users_role_id_foreign    FK CONSTRAINT     �   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.role(id) ON DELETE CASCADE;
 E   ALTER TABLE ONLY public.users DROP CONSTRAINT users_role_id_foreign;
       public       postgres    false    199    202    2727                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   2869.dat                                                                                            0000600 0004000 0002000 00000000025 13522244430 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	pria
2	wanita
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2862.dat                                                                                            0000600 0004000 0002000 00000000531 13522244430 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_06_041111_create_role_table	2
4	2014_10_12_000001_create_users_table	3
5	2019_08_06_043640_create_gender_table	4
6	2019_08_06_043800_create_study_table	5
7	2019_08_06_043844_create_status_table	6
8	2019_08_06_044037_create_survey_table	7
\.


                                                                                                                                                                       2865.dat                                                                                            0000600 0004000 0002000 00000000005 13522244430 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           2867.dat                                                                                            0000600 0004000 0002000 00000000034 13522244430 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	moderator
2	surveyor
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    2873.dat                                                                                            0000600 0004000 0002000 00000000075 13522244430 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Belum Diverifikasi
2	Verifikasi
3	Tidak terverifikasi
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                   2871.dat                                                                                            0000600 0004000 0002000 00000000052 13522244430 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	SD
2	SMP
3	SMA
4	DIPLOMA
5	SARJANA
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      2875.dat                                                                                            0000600 0004000 0002000 00000000243 13522244430 0014255 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        5	Rafi'	33444	1565740800	1	4	1
6	Tangguh	334441	1565308800	1	5	1
2	Dio	21233	1565827200	2	1	2
3	faris	200033	1546387200	1	4	3
4	Haikal	23300	1564790400	1	4	2
\.


                                                                                                                                                                                                                                                                                                                                                             2864.dat                                                                                            0000600 0004000 0002000 00000000611 13522244430 0014252 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	Tatang Suherman	moderator	$2y$10$nZ3N7eVRtQrTWsi98TFWG.wJLVVsbXFY1FiG2dVmeCqNdD9FsKMqm	z1VTw1RRgSuTGllziObnSvwSJBSs1kTQ9tNj1zEJBETy8HRFzuhRuLxFcOI3	2019-08-06 04:30:16	2019-08-06 04:30:16	1
3	Faris Rafi' Abdillah	surveyor	$2y$10$Ac0voqryL/6CfuiTEC8mYOOF5k2aGJfGV0IVGzDFelEfCviYEVDh2	B4BHc7sJcmj5PvGjAhARrCeUtyIBcTs9WzWyeDSTuajdKjIZA4EaclIUfgfm	2019-08-06 05:52:36	2019-08-06 05:52:36	2
\.


                                                                                                                       restore.sql                                                                                         0000600 0004000 0002000 00000034712 13522244430 0015372 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.7
-- Dumped by pg_dump version 10.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.users DROP CONSTRAINT users_role_id_foreign;
ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_study_id_foreign;
ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_status_id_foreign;
ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_gender_id_foreign;
DROP INDEX public.password_resets_email_index;
ALTER TABLE ONLY public.users DROP CONSTRAINT users_username_unique;
ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
ALTER TABLE ONLY public.survey DROP CONSTRAINT survey_pkey;
ALTER TABLE ONLY public.study DROP CONSTRAINT study_pkey;
ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
ALTER TABLE ONLY public.migrations DROP CONSTRAINT migrations_pkey;
ALTER TABLE ONLY public.gender DROP CONSTRAINT gender_pkey;
ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.survey ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.study ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.status ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.migrations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.gender ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.users_id_seq;
DROP TABLE public.users;
DROP SEQUENCE public.survey_id_seq;
DROP TABLE public.survey;
DROP SEQUENCE public.study_id_seq;
DROP TABLE public.study;
DROP SEQUENCE public.status_id_seq;
DROP TABLE public.status;
DROP SEQUENCE public.role_id_seq;
DROP TABLE public.role;
DROP TABLE public.password_resets;
DROP SEQUENCE public.migrations_id_seq;
DROP TABLE public.migrations;
DROP SEQUENCE public.gender_id_seq;
DROP TABLE public.gender;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: gender; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gender (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.gender OWNER TO postgres;

--
-- Name: gender_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gender_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gender_id_seq OWNER TO postgres;

--
-- Name: gender_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gender_id_seq OWNED BY public.gender.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- Name: role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;


--
-- Name: status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.status OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_id_seq OWNER TO postgres;

--
-- Name: status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;


--
-- Name: study; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.study (
    id integer NOT NULL,
    description character varying(255) NOT NULL
);


ALTER TABLE public.study OWNER TO postgres;

--
-- Name: study_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.study_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.study_id_seq OWNER TO postgres;

--
-- Name: study_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.study_id_seq OWNED BY public.study.id;


--
-- Name: survey; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.survey (
    id integer NOT NULL,
    nama character varying(255) NOT NULL,
    nik bigint NOT NULL,
    tanggal integer NOT NULL,
    gender_id integer NOT NULL,
    study_id integer NOT NULL,
    status_id integer NOT NULL
);


ALTER TABLE public.survey OWNER TO postgres;

--
-- Name: survey_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_id_seq OWNER TO postgres;

--
-- Name: survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.survey_id_seq OWNED BY public.survey.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    role_id integer NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: gender id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gender ALTER COLUMN id SET DEFAULT nextval('public.gender_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);


--
-- Name: status id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);


--
-- Name: study id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.study ALTER COLUMN id SET DEFAULT nextval('public.study_id_seq'::regclass);


--
-- Name: survey id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey ALTER COLUMN id SET DEFAULT nextval('public.survey_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: gender; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gender (id, description) FROM stdin;
\.
COPY public.gender (id, description) FROM '$$PATH$$/2869.dat';

--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
\.
COPY public.migrations (id, migration, batch) FROM '$$PATH$$/2862.dat';

--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.
COPY public.password_resets (email, token, created_at) FROM '$$PATH$$/2865.dat';

--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role (id, description) FROM stdin;
\.
COPY public.role (id, description) FROM '$$PATH$$/2867.dat';

--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status (id, description) FROM stdin;
\.
COPY public.status (id, description) FROM '$$PATH$$/2873.dat';

--
-- Data for Name: study; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.study (id, description) FROM stdin;
\.
COPY public.study (id, description) FROM '$$PATH$$/2871.dat';

--
-- Data for Name: survey; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.survey (id, nama, nik, tanggal, gender_id, study_id, status_id) FROM stdin;
\.
COPY public.survey (id, nama, nik, tanggal, gender_id, study_id, status_id) FROM '$$PATH$$/2875.dat';

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, username, password, remember_token, created_at, updated_at, role_id) FROM stdin;
\.
COPY public.users (id, name, username, password, remember_token, created_at, updated_at, role_id) FROM '$$PATH$$/2864.dat';

--
-- Name: gender_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gender_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 8, true);


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_id_seq', 1, false);


--
-- Name: status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_id_seq', 1, false);


--
-- Name: study_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.study_id_seq', 1, false);


--
-- Name: survey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.survey_id_seq', 6, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 3, true);


--
-- Name: gender gender_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- Name: study study_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.study
    ADD CONSTRAINT study_pkey PRIMARY KEY (id);


--
-- Name: survey survey_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_unique UNIQUE (username);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: survey survey_gender_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_gender_id_foreign FOREIGN KEY (gender_id) REFERENCES public.gender(id) ON DELETE CASCADE;


--
-- Name: survey survey_status_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_status_id_foreign FOREIGN KEY (status_id) REFERENCES public.status(id) ON DELETE CASCADE;


--
-- Name: survey survey_study_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_study_id_foreign FOREIGN KEY (study_id) REFERENCES public.study(id) ON DELETE CASCADE;


--
-- Name: users users_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.role(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      