<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('graph');
});
Route::get('/graph', function () {
    return view('graph');
})->name('graph')->middleware('auth');
Route::resource('survey', 'SurveyController')->middleware('auth');
Route::get('/survey/{action}/{id}', 'SurveyController@validasi')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
